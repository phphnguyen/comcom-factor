var LocationsController = function(the_app){

	this.app 	= the_app;
	this.slug 	= "/locations";

	this.routes = {
		"/" 	: {
			call_back	: "getAllLocations",
			methods 	: ['get', 'post']

		}	

	};//end of route



	//=======================================
	// Function to get location s
	//=======================================
	this.getAllLocations = function(req, res, the_man){

		console.log("checking this pointer ", the_man);

	};//end of get all locations


};//


exports.controllers = function(app){

	return new LocationsController(app);

};//end of controllers
