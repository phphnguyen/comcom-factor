var ThreadsController = function(the_app){

	this.app 	= the_app;
	this.model 	= require('../models/threads-model');
	this.slug 	= "/threads";

	this.routes = {
		"/" 	: {
			call_back	: "getAllThreads",
			methods 	: ['get', 'post']

		}	

	};//end of route



	this.init = function(){




	};//end of function init

	this.getAllThreads = function(){

		return [];

	};//end of function


};//end of threads controller


exports.controllers = function(app){

	return new ThreadsController(app);

};//end of controllers
