 var RouteDriver = function(the_app){

 	this.settings = {
 		app: the_app,
 		controllers: "controllers"
 	};
 	this.init = function(){

 		this.loadController();


 	};//endof init function


 	//==================================
 	// Function to load controller
 	//==================================
 	this.loadController = function(){

 		var the_man = this;
 		var app = the_man.settings.app;

 		var fs = require('fs');


 		var _c_files = fs.readdirSync("./" + the_man.settings.controllers);
 		
 		_c_files.forEach(function(_file){

 			var route = require("../" + the_man.settings.controllers + "/" + _file.replace('.js',''));
 			

 			console.log("working with " + _file);
 			//init controller so why not loading routes here
 			the_man.loadRoutes(route.controllers(the_man.settings.app)); 


 		});

 	};//endof loading contorller




 	//====================================
 	//loading all routes
 	//====================================
 	this.loadRoutes = function(_control){

 		var the_man = this;
 		var app = the_man.settings.app;

 		if(typeof _control.routes !== 'undefined'){

 			for(var _path in _control.routes){

 				_control.routes[_path].methods.forEach(function(_method){

 					if(_method == 'get'){
 						app.get(_control.slug + _path , function(req, res){
 							 _control[_control.routes[_path].call_back].apply(null, [req, res, _control]);
 						});
 					}

 				});

 			}

 		}



 	};//end of controller







 };//end of route driver






/*
 * GET home page.
 */
exports.controllers = function(app){
	var route_driver = new RouteDriver(app);
	route_driver.init();


	return route_driver;
};//end of controllers

